﻿using System;

namespace ForLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Looping through strings in an array");

            string[] myWords = { "one", "two", "three", "four", "five", "six", "seven", "eleven" };

            Console.WriteLine($"My array is: {myWords}");

            foreach (string i in myWords)
            {
                string length = Convert.ToString(i.Length);
                Console.WriteLine($"The word we have in this loop is \'{i}\' and it is \'{length}\' characters in length");
            }

            Console.WriteLine();

            Console.WriteLine("Looping through the same array with a different expression");
            for (int i = 0; i < myWords.Length; i++)
            {
                Console.WriteLine($"Index \'{Convert.ToString(i)}\' is {myWords[i]}");
            }

            Console.WriteLine();

            Console.WriteLine("Looping through numbers 1 to 10");

            for (int i = 0; i <= 10; i++)
            {
                Console.WriteLine($"This is iteration number \'{Convert.ToString(i)}\'");
            }

            Console.WriteLine("The program has ended now. Press enter to close...");

            Console.ReadLine();
        }
    }
}
