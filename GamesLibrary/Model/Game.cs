﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GamesLibrary.Model
{
    public class Game
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public string GameName { get; set; }

        public string DevelopmentStudio { get; set; }
    }
}
