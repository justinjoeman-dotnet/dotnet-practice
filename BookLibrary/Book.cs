﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary
{
    class Book
    {
        public string title;
        public int pages;
        public string author;
        public bool available;

        public static int bookCount = 0;

        public Book(string aTitle, int aPages, string aAuthor, bool aAvailable)
        {
            title = aTitle;
            pages = aPages;
            author = aAuthor;
            available = aAvailable;
            Console.WriteLine($"Creating book object with following details: {title}, {pages}, {author}, available: {available}");
            bookCount++;
        }

        public bool CheckAvailability()
        {
            return available;
        }

        public bool LoanBook()
        {
            
            if (CheckAvailability())
            {
                Console.WriteLine($"Loaning \'{title}\' as requested");
                available = false;
            }
            else if (CheckAvailability() == false)
            {
                Console.WriteLine($"Book \'{title}\' is already out on loan.");
            }
            return available;
        }

        public bool ReturnBook()
        {
            if (CheckAvailability() == false)
            {
                Console.WriteLine($"Book \'{title}\' returned to library");
                available = true;
            }
            else if (CheckAvailability() == true)
            {
                Console.WriteLine($"Oh! Either we have a duplicate copy of \'{title}\' it was just misplaced in the library. Nothing to do here.");
            }

            return available;
        }
    }
}
