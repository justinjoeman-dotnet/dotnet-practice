﻿using System;

namespace BookLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Justin's library. Lets create some books - we will create methods for this later");

            Book book1 = new Book("To Kill A Mockingbird", 400, "Harper Lee", true);
            Console.WriteLine($"The number of books created for the library is: {Book.bookCount}");
            Book book2 = new Book("Harry Potter", 300, "JK Rowling", false);
            Console.WriteLine($"The number of books created for the library is: {Book.bookCount}");
            Book book3 = new Book("Life with Lana - An Anxiety Diary", 200, "Lana Hood", false);
            Console.WriteLine($"The number of books created for the library is: {Book.bookCount}");

            Console.WriteLine("Okay let's borrow a book");
            book1.LoanBook();

            Console.WriteLine("Okay let's borrow another book");
            book2.LoanBook();

            Console.WriteLine("I think we better return a book...");
            book3.ReturnBook();

            Console.WriteLine("Press enter to close");
            Console.ReadLine();
        }
    }
}
