﻿using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {

            User user1 = new User(GetName(), GetAge(), false, "1");
            User user2 = new User(GetName(), GetAge(), false, "I'm an alien, what I need a grade for?");

            WelcomeUser(user1.name, user1.age);

            user1.student = RegisterStudent(user1.student);

            user1.UpdateName();

            Console.WriteLine(user1.name);

            Console.WriteLine($"{user2.grade}");

            int num = GetNum();

            SayQuote(num);

            Console.WriteLine("We are going to test a while loop now... hope we don't get stuck!");
            ShowMenu();
            int selection = MenuSelection();

            while (selection != 0)
            {
                ShowMenu();
                selection = MenuSelection();
            }

            Console.Write("Thank you. Press enter to close program");

            Console.ReadLine();

        }

        static string GetName()
        {
            Console.Write("Type your name and press enter: ");
            string userName = Console.ReadLine();
            return userName;
            
            
        }

        static string GetAge()
        {
            Console.Write("Type your age and press enter: ");
            string age = Console.ReadLine();
            return age;
            
        }

        static bool RegisterStudent(bool currentStatus)
        {
            bool status = currentStatus;
            
            Console.WriteLine($"Currently registered student: {currentStatus}");
            Console.Write("Enter \'1\' to register, \'2\' to not register and press enter: ");
            string input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    status = true;
                    break;
                case "2":
                    status = false;
                    break;
            }

            if (status)
            {
                Console.WriteLine("You are a registered student");
            } else if (status == false)
            {
                Console.WriteLine("You are not a registered student");
            } else if (currentStatus == status)
            {
                Console.WriteLine($"No changes requred as your registration status is already {status}");
            }

            return status;
        }

        static void WelcomeUser(string name, string age)
        {
            Console.WriteLine($"Welcome {name}! Let's get you registered...");
        }


        static int GetNum()
        {
            int quote;

            Console.Write("Type a number between 0 - 3 and press enter: ");
            
            quote = Convert.ToInt32(Console.ReadLine());
            
            return quote;
        }

        static void SayQuote(int num)
        {
            switch (num)
            {
                case 0:
                    Console.WriteLine("OoOoOooWeeeee");
                    break;
                case 1:
                    Console.WriteLine("Have a good day");
                    break;
                case 2:
                    Console.WriteLine("Have an excellent day");
                    break;
                case 3:
                    Console.WriteLine("Have THE BEST day");
                    break;
            }
        }

        static void ShowMenu()
        {
            Console.WriteLine("**************************");
            Console.WriteLine();
            Console.WriteLine("Type '0' to exit this loop");
            Console.WriteLine();
            Console.WriteLine("**************************");
        }

        static int MenuSelection()
        {
            int selection;

            selection = Convert.ToInt32(Console.ReadLine());

            return selection;
        }

    }

}


