﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld
{
    class User
    {
        public string name;
        public string age;
        public bool student;
        public string grade;

        public User(string aName, string aAge, bool aStudent, string aGrade)
        {
            name = aName;
            age = aAge;
            student = aStudent;
            Grade = aGrade;
            Console.WriteLine($"Creating object with following details: {name}, {age}, {student}, {grade}");
        }

        public string UpdateName()
        {
            Console.Write($"We currently have your name as {name} - Please enter a new name: ");
            string newName = Console.ReadLine();

            Console.WriteLine($"You have entered \'{newName}\'. Press \'1\' to confirm or \'2\' to cancel: ");
            string confirmation = Console.ReadLine();

            if (confirmation == "1")
            {
                name = newName;
            } 
            else if (confirmation == "2")
            {
                Console.WriteLine("Cancelling changes...");
            }
            else
            {
                Console.WriteLine("Invalid entry");
            }

            return name;
        }

        private string Grade
        {
            get { return grade;  }
            set { 
                if (value == "1" || value == "2" || value == "3")
                {
                    grade = value;
                }
                else
                {
                    grade = "other";
                }
            }
        }
    }
}

    
