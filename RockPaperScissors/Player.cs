﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RockPaperScissors
{
    class Player
    {
        public string name;
        public int score;

        public Player(string aName, int aScore)
        {
            name = aName;
            score = aScore;
        }

    }
}
