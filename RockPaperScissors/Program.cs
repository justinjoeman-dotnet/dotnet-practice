﻿using System;
using System.Threading;



namespace RockPaperScissors
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to  the \'Rock Paper Scissors\' game!".ToUpper());
            
            Console.Write("Type your name: ");
            
            string playerName = Console.ReadLine();

            Game game = new Game();
            
            Player user = new Player(playerName, 0);
            
            Player opponent = new Player("AI", 0);

            Console.WriteLine($"So {user.name}... first to 3 wins. Lets play!\n");

            string playerAnswer;

            string opponentAnswer;

            do
            {
                playerAnswer = game.GetUserInput();
                opponentAnswer = game.GetOpponentInput();

                Console.WriteLine($"{user.name} chose \'{playerAnswer}\'");
                Console.WriteLine($"{opponent.name} chose \'{opponentAnswer}\'\n");

                int result = game.CompareAnswers(playerAnswer, opponentAnswer);

                switch (result)
                {
                    case 1:
                        user.score++;
                        break;
                    case 2:
                        opponent.score++;
                        break;
                    case 3:
                        break;
                }

                Console.WriteLine("Current scores: ");
                Console.WriteLine($"{user.name}: {user.score}");
                Console.WriteLine($"{opponent.name}: {opponent.score}\n");

                if (user.score == 3 || opponent.score == 3)
                {
                    break;
                }
            }
            while (user.score < 3 || opponent.score < 3);

            game.CheckWinner(user.score, opponent.score);

            Thread.Sleep(5000);
            
        }
        
    }
}
