﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RockPaperScissors
{
    public class Game
    {
        public string GetUserInput()
        {
            Console.WriteLine("Press \'1\' for \'Rock\'\nPress \'2\' for \'Scissors\'\nPress \'3\' for \'Paper\' \nand press \'Enter\'\n");

            string input = Console.ReadLine();
            string answer;

            switch (input)
            {
                case "1":
                    answer = "Rock";
                    break;
                case "2":
                    answer = "Scissors";
                    break;
                case "3":
                    answer = "Paper";
                    break;
                default:
                    Console.WriteLine($"\'{input}\' is not a valid answer. Generating a random choice for you");
                    string[] myArray = { "Rock", "Paper", "Scissors" };
                    Random autogen = new Random();
                    answer = myArray[autogen.Next(0, 3)];
                    break;
            }

            return answer;
        }


        public string GetOpponentInput()
        {
            Random random = new(); // simplified way of calling the class

            string input = Convert.ToString(random.Next(1, 4));
            string answer;

            switch (input)
            {
                case "1":
                    answer = "Rock";
                    break;
                case "2":
                    answer = "Scissors";
                    break;
                case "3":
                    answer = "Paper";
                    break;
                default:
                    Console.WriteLine($"\'{input}\' is not a valid answer. Generating a random choice for you");
                    string[] myArray = { "Rock", "Paper", "Scissors" };
                    Random autogen = new Random();
                    answer = myArray[autogen.Next(0, 3)];
                    break;
            }

            return answer;
        }

        public int CompareAnswers(string playerAnswer, string opponentAnwser)
        {
            // 1 is a player win
            // 2 is an opponent win
            // 3 is a draw
            int result;

            if (playerAnswer == opponentAnwser)
            {
                Console.WriteLine("DRAW! No points awarded");
                result = 3;
            }
            else if ((playerAnswer == "Rock" && opponentAnwser == "Scissors") || (playerAnswer == "Scissors" && opponentAnwser == "Paper") || (playerAnswer == "Paper" && opponentAnwser == "Rock"))
            {
                Console.WriteLine("Player wins!");
                result = 1;
            }
            else
            {
                Console.WriteLine("Opponent wins!");
                result = 2;
            }

            return result;
        }

        public void CheckWinner(int playerScore, int opponentScore)
        {
            if (playerScore == 3 && opponentScore < 3)
            {
                Console.WriteLine("Player wins the game! Good job! Closing in 5 Seconds...");
            }
            else if (playerScore < 3 && opponentScore == 3)
            {
                Console.WriteLine("Opponent wins! Better luck next time. Closing in 5 Seconds...");
            }

        }
    }
}
